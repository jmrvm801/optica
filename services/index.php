<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set("America/Mexico_City");
include_once('db.php');
include_once('gral.php');
include_once('Util.php');
$runsql = array();
foreach (glob("controllers/*.php") as $file){
	include_once($file);
	$class = explode('.', explode('/', $file)[1])[0];
	$array = array('QueryBuilder', 'SqlManagement', 'MarssoftError');
	if (array_search($class, $array) === false)
		if (count(class_implements($class)) > 0)
			array_push($runsql, $class);
}

class Ws extends Util
{
	public static $g = '';
	public static $c = '';

	public function __construct($runsql){
		Ws::$g = new Gral();
		Ws::$c = new db();
		$this->genCookies();
		for($i = 0; $i < count($runsql); $i++){
			$rule = (new ReflectionClass($runsql[$i]))->newInstanceArgs(array());
			$rule->sql_rules();
		}
	}

	public function genCookies(){
		foreach ($_COOKIE as $clave => $valor)
			Ws::$g->cook($clave, $valor, 7200);
	}

	public function run($class){
		$login = new Login();
		switch ($class) {
			case 'login':
				Ws::$g->kill(json_encode($login->run($_POST['method'])));
				break;
			case 'migration':
				Ws::migration();
			break;
			default:
				if (!$login->logged())
					Ws::$g->kill(json_encode(array('denied')));
				if ($_SESSION['use_profile'] == '3')
					Ws::$g->kill(json_encode(array('denied')));
				$reflect = (new ReflectionClass($class))->newInstanceArgs(array());
				Ws::$g->kill(json_encode($reflect->run($_POST['method'])));
				break;
		}
	}
	public static function migration(){
		Ws::e('Iniciando servicio de migración');
		// $product = new Products();
		// $data = file_get_contents('file.json');
		// $data = json_decode($data, true);
		// for($i = 0; $i < count($data); $i++){
		// 	$simple = $data[$i];
		// 	$simple['images'] = $simple['docs'];
		// 	unset($simple['docs']);
		// 	unset($simple['aaa']);
		// 	$simple['pro_bra_id'] = '6';
		// 	Ws::$c->q("SELECT sub_id FROM subfamilies WHERE sub_name = '$simple[pro_sub_id]' LIMIT 1;");
		// 	$simple['pro_sub_id'] = Ws::$c->r();
		// 	$result = $product->create($simple);
		// 	Ws::e($result);
		// }
	}

	public static function e($value = ''){
		if (is_array($value)){
			echo '<pre>';
			print_r($value);
			echo '</pre>';
		} else {
			echo date('Y-m-d H:i.s').':- '.$value.'<br>';
		}
	}

	public function __destruct(){
		if (Ws::$c != '')
			Ws::$c->cl();
	}
}
//Ws::$g->sendmail('ejemplo@correo.com', 'Hola mundo', 'Saludos');
if (isset($_POST['class'])) 
	(new Ws($runsql))->run($_POST['class']);
else
	(new Ws($runsql))->run('migration');