<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Families extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }

  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        return $this->update($_POST['data']);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
    }
  }

  public function delete($id){
    Logs::createRecord('', 18, "Se eliminó la familia $id");
    $this->remove($id);
    return Gral::response('true');
  }

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $_SESSION['fam_id'] = $id;
    $single = Ws::$c->fa();
    $single = $this->utf8_client($single);
    return $single;
  }

  public function get(){
    $this->sget(Ws::$c, "", "fam_name");
    $array = array();
    while($row = Ws::$c->fa()){
      $row = $this->utf8_client($row);
      array_push($array, $row);
    }
    return $array;
  }

  public function update($data){
    $data = $this->utf8_server($data);
    try {
      $this->upd($_SESSION["fam_id"], $data);
      Logs::createRecord('', 17, "Se editó la familia $data[fam_name]");
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  public function create($data){
    $data = $this->utf8_server($data);
    try {
      $id = $this->insert("NULL", $data);
      Logs::createRecord('', 16, "Se creó la familia $data[fam_name]");
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function sql_rules(){
    $this->create_table();
    $this->create_fields($this->foreign_keys, 0, true);
    $this->create_fields($this->rows, 0);
  }
  /**
   * Set foreign keys
   */
  public $foreign_keys = array();
  /**
   * Set row keys
   */
  public $rows = array(
    array('name', 'varchar(100)', 'NOT NULL'),
    array('deleted', 'varchar(100)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>