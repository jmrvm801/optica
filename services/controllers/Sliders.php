<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Sliders extends QueryBuilder implements SqlManagement{
  public $status = array('', 'Categoría', 'Producto');

  public function __construct(){
    parent::__construct($this);
  }
  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        return $this->update($_POST['data']);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
    }
  }

  public function getActiveSliders(){
    $array = array();
    $sliders = $this->get();
    for($i = 0; $i < count($sliders); $i++){
      $slider = $sliders[$i];
      if ($slider['status'] == 'Publicada')
        array_push($array, $slider);
    }
    return $sliders;
  }


  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $single = Ws::$c->fa();
    $single = $this->utf8_client($single);
    $_SESSION['sli_id'] = $id;
    $single['sli_start'] = Ws::$g->numberDateToString($single['sli_start']);
    $single['sli_end'] = Ws::$g->numberDateToString($single['sli_end']);
    return $single;
  }

  public function getStatus($start, $end){
    $start = date('Ymd', strtotime($start));
    $end = date('Ymd', strtotime($end));
    $today = date('Ymd');
    return ($today >= $start && $today <= $end);
  }

  public function get(){
    $d = new db();
    $sub = new Subfamilies();
    $product = new Products();
    $this->sget($d, "", "sli_id DESC");
    $array = array();
    while($row = $d->fa()){
      $row = $this->utf8_client($row);
      $row['status'] = ($this->getStatus($row['sli_start'], $row['sli_end'])) ? 'Publicada' : 'Sin publicar';
      $row['goto'] = $this->status[$row['sli_goto']];
      if ($row['sli_goto'] == '1'){
        $row['subfamily'] = $sub->single($row['sli_sub_id']);
      } else {
        $row['sli_pro_id'] = json_decode($row['sli_pro_id']);
        if (count($row['sli_pro_id']) == 1)
          $row['product'] = $product->single($row['sli_pro_id']);
        else {
          $row['product'] = array(
            'pro_code' => '',
            'pro_name' => 'Varios productos',
          );
        }
      }
      array_push($array, $row);
    }
    $d->cl();
    return $array;
  }
  public function update($data){
    $data = $this->utf8_server($data);
    $data['sli_start'] = Ws::$g->stringDateToNumber($data['sli_start'], '-');
    $data['sli_end'] = Ws::$g->stringDateToNumber($data['sli_end'], '-');
    if (isset($data['images'])){
      $document = $data['images'];
      unset($data['images']);
      $data['sli_extension'] = $document['doc_type'];
    } else
      $document = '';
    if (isset($data['sli_pro_id'])){
      $data['sli_pro_id'] = json_encode($data['sli_pro_id']);
    }
    try {
      $this->upd($_SESSION['sli_id'], $data);
      if ($document != ''){
        $this->setFile($document, $_SESSION['sli_id'], 'sliders');
      }
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  public function create($data){
    $data = $this->utf8_server($data);
    $document = $data['images'];
    unset($data['images']);
    if (isset($data['sli_pro_id'])){
      $data['sli_pro_id'] = json_encode($data['sli_pro_id']);
    }
    $data['sli_start'] = Ws::$g->stringDateToNumber($data['sli_start'], '-');
    $data['sli_end'] = Ws::$g->stringDateToNumber($data['sli_end'], '-');
    $data['sli_extension'] = $document['doc_type'];
    $number = Ws::$g->comparateDates($data['sli_start'], $data['sli_end']);
    if ($number == 2){
      return Gral::error('minor');
    }
    try {
      $id = $this->insert("NULL", $data);
      $this->setFile($document, $id, 'sliders');
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id); 
  }

  public function sql_rules(){
    $this->create_table();
    $this->create_fields($this->foreign_keys, 0, true);
    $this->create_fields($this->rows, 0);
  }
  /**
   * Set foreign keys
   */
  public $foreign_keys = array(
    array('sub_id', 'int(6)', 'NOT NULL'),
    array('pro_id', 'TEXT', 'NOT NULL'),
  );
  /**
   * Set row keys
   */

  public $rows = array(
    array('alignment', 'varchar(100)', 'NOT NULL'),
    array('title', 'varchar(100)', 'NOT NULL'),
    array('descr', 'varchar(100)', 'NOT NULL'),
    array('extension', 'varchar(100)', 'NOT NULL'),
    array('goto', 'varchar(100)', 'NOT NULL'), /* 1 = go_category, 2 = go_product  */
    array('extension', 'varchar(5)', 'NOT NULL'),

    array('start', 'date', 'NOT NULL'),
    array('end', 'date', 'NOT NULL'),

    array('deleted', 'varchar(100)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>