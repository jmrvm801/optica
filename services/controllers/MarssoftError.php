<?php 

/**
 * Class who controlls Errors on PHP
 * 
 * @author Fernando Carreón
 */
  class MarssoftError extends \Exception{
    private $_options;

    /**
     * Construct
     * 
     * @param String $message to show
     * @param Array $options to show
     * @param Integer $code to show
     * @param Exception $previous error generated
     */

    public function __construct($message = '', $options = array(), $code = 0, Exception $previous = null){
      parent::__construct($message, $code, $previous);
      $this->_options = $options;
    }

    /**
     * Get private options
     * 
     * @return Array $options
     */

    public function getOptions(){
      return $this->_options;
    }
  }
?>