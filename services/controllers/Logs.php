<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Logs extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }
  public function run($method = 'default'){
    switch($method){
      case 'createRecord':
        return $this->createRecord('',$_POST['action'],$_POST['complemento']);
      break;
    }
  }

  /*
  1. Inicio de sesión
  2. Ingreso incorrecto de contraseña
  3. Bloqueo por accesos erróneos en el sistema
  4. Cierre de sesión
  5. Usuario creado
  6. Usuario eliminado.
  7. Permiso denegado a una sección
  8. Usuario editado
  9. Cambio de contraseña
  10. Cliente creado
  11. Cliente modificado
  12. Cliente eliminado
  13. Proveedor creado
  14. Proveedor modificado
  15. Proveedor eliminado
  16. Familia creada
  17. Familia modificada
  18. Familia eliminada
  19. Subfamilia creada
  20. Subfamilia modificada
  21. Subfamilia eliminada
  22. Subfamilia creada
  23. Subfamilia modificada
  24. Subfamilia eliminada
  */
  public static function createRecord($user = '', $action, $complement = ""){
    $complement = utf8_decode($complement);
    if ($user == '' && isset($_SESSION['use_id']))
      $user = $_SESSION['use_id'];
    Ws::$c->q("INSERT INTO logs VALUES(NULL, '$user', '$action', '$complement', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."')");
  }

  /* SQL RULES */

  public function sql_rules(){
    $this->create_table(0);
    $foreign_keys = array(
      array('log_use_id', 'varchar(30)', 'NOT NULL')
    );
    $this->create_fields($foreign_keys, 0, true);
    $rows = array(
      array('type', 'int(2)', 'NOT NULL'),
      array('complement', 'varchar(200)', 'NULL'),
      array('created_at', 'datetime', 'NOT NULL'),
      array('updated_at', 'datetime', 'NOT NULL')
    );
    $this->create_fields($rows, 0);
  }
}

?>