<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Features extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }
  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        return $this->update($_POST['id'], $_POST['data']);
      case 'get':
        return $this->get($_POST['id']);
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
    }
  }

  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  public function single($id, $price = false){
    $this->bsingle(Ws::$c, $id);
    $single = Ws::$c->fa();
    $single = $this->utf8_client($single);
    if ($single['pro_offer_price'] != '' && $single['pro_offer_price'] != '0'){
      $original = $single['pro_price_1'];
      $single['pro_price_1'] = $single['pro_offer_price'];
      $single['pro_offer_price'] = $original;
    }
    if ($price){
      $base = array('pro_price_1', 'pro_price_3', 'pro_price_6', 'pro_price_9', 'pro_price_12', 'pro_offer_price');
      for($i = 0; $i < count($base); $i++){
        $item = $single[$base[$i]];
        if ($item == '' || $item == '0')
          continue;
        $single[$base[$i]] = $single[$base[$i]] + (($single[$base[$i]] * 0.0290) + 2.50) * 1.16;
      }
    }
    
    $single['pictures'] = (new Documents())->getDocuments('products', $id);
    $single['subfamily'] = (new Subfamilies())->single($single['pro_sub_id']);
    return $single;
  }

  public function get($id){
    $d = new db();
    $this->sget($d, "fea_pro_id = '$id'", "fea_id");
    $array = array();
    while($row = $d->fa()){
      $row = $this->utf8_client($row);
      array_push($array, $row);
    }
    $d->cl();
    return $array;
  }
  public function update($id, $data){
    $data = $this->utf8_server($data);
    try {
      $this->upd($id, $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  public function create($data){
    $data = $this->utf8_server($data);
    try {
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id); 
  }

  public function sql_rules(){
    $this->create_table();
    $this->create_fields($this->foreign_keys, 0, true);
    $this->create_fields($this->rows, 0);
  }
  /**
   * Set foreign keys
   */
  public $foreign_keys = array(
    array('pro_id', 'int(6)', 'NOT NULL'),
  );
  /**
   * Set row keys
   */
  public $rows = array(
    array('title', 'varchar(100)', 'NOT NULL'),
    array('descr', 'TEXT', 'NOT NULL'),

    array('deleted', 'varchar(100)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>