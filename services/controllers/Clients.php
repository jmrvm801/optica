<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Clients extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }

  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      break;
      case 'update':
        return $this->update($_POST['data']);
      break;
      case 'get':
        return $this->get();
      break;
      case 'single':
        return $this->single($_POST['id']);
      break;
      case 'delete':
        return $this->delete($_POST['id']);
      break;
    }
  }

  public function delete($id){
    $client = $this->single($id);
    Logs::createRecord('', 12, "Se eliminó al cliente $client[cli_social_reason]");
    Ws::$c->q("UPDATE clients SET cli_deleted = '1', cli_updated_at = '".date('Y-m-d H:i:s')."' WHERE cli_id = '$id' LIMIT 1;");
    return array('response' => 'true');
  }

  public function single($id){
    Ws::$c->q("SELECT * FROM clients WHERE cli_id = '$id' LIMIT 1;");
    $client = Ws::$c->fa();
    $_SESSION['cli_id'] = $client['cli_id'];
    $client = $this->utf8_client($client);
    return $client;
  }

  public function get(){
    Ws::$c->q("SELECT cli_id, CONCAT_WS(' ', cli_last_name, cli_sur_name, cli_first_name) AS fullname, cli_social_reason, cli_rfc, cli_email, cli_phone FROM clients WHERE cli_deleted = '0' ORDER BY cli_social_reason");
    $clients = array();
    while($client = Ws::$c->fa()){
      $client = $this->utf8_client($client);
      array_push($clients, $client);
    }
    return $clients;
  }
  public function update($data){
    $data = $this->utf8_server($data);
    $data['cli_birthday'] = date('Y-m-d', strtotime(Ws::$g->stringDateToNumber($data['cli_birthday'])));
    Ws::$c->q("UPDATE clients SET cli_first_name = '$data[cli_first_name]', cli_last_name = '$data[cli_last_name]', cli_sur_name = '$data[cli_sur_name]', cli_gender = '$data[cli_gender]', cli_birthday = '$data[cli_birthday]', cli_address = '$data[cli_address]', cli_postal_code = '$data[cli_postal_code]', cli_colony = '$data[cli_colony]', cli_city = '$data[cli_city]', cli_state = '$data[cli_state]', cli_country = '$data[cli_country]', cli_social_reason = '$data[cli_social_reason]', cli_rfc = '$data[cli_rfc]', cli_phone = '$data[cli_phone]', cli_email = '$data[cli_email]', cli_updated_at = '".date('Y-m-d H:i:s')."' WHERE cli_id = $_SESSION[cli_id] LIMIT 1;");
    Logs::createRecord('', 11, "Se editó al cliente No. $_SESSION[cli_id]");
    return array('response' => 'true');
  }
  public function create($data){
    $data = $this->utf8_server($data);
    $data['cli_birthday'] = date('Y-m-d', strtotime(Ws::$g->stringDateToNumber($data['cli_birthday'])));
    Ws::$c->q("INSERT INTO clients VALUES(NULL, '$data[cli_first_name]', '$data[cli_last_name]', '$data[cli_sur_name]', '$data[cli_gender]', '$data[cli_birthday]', '$data[cli_address]', '$data[cli_postal_code]', '$data[cli_colony]', '$data[cli_city]', '$data[cli_state]', '$data[cli_country]', '$data[cli_social_reason]', '$data[cli_rfc]', '$data[cli_phone]', '$data[cli_email]', '0', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."')");
    $last = Ws::$c->last('clients');
    Logs::createRecord('', 10, "Se creó al cliente No. ".$last);
    return array('response' => 'true');
  }

  public function sql_rules(){
    $this->create_table(0);
    $foreign_keys = array(
      
    );
    $this->create_fields($foreign_keys, 0, true);
    $rows = array(
      array('first_name', 'varchar(100)', 'NOT NULL'),
      array('last_name', 'varchar(100)', 'NOT NULL'),
      array('sur_name', 'varchar(100)', 'NOT NULL'),
      array('gender', 'varchar(100)', 'NOT NULL'),
      array('birthday', 'date', 'NOT NULL'),
      array('address', 'varchar(200)', 'NOT NULL'),

      array('postal_code', 'varchar(5)', 'NULL'),
      array('colony', 'varchar(200)', 'NULL'),
      array('city', 'varchar(200)', 'NULL'),
      array('state', 'varchar(100)', 'NULL'),
      array('country', 'varchar(50)', 'NULL'),
      array('social_reason', 'varchar(200)', 'NULL'),
      array('rfc', 'varchar(15)', 'NULL'),
      array('phone', 'varchar(10)', 'NULL'),
      array('email', 'varchar(200)', 'NULL'),

      array('deleted', 'int(1)', 'NOT NULL'),
      array('created_at', 'datetime', 'NOT NULL'),
      array('updated_at', 'datetime', 'NOT NULL')
    );
    $this->create_fields($rows, 0);
  }
}

?>