String.prototype.formatMoney = function (c, d, t) {
    return parseFloat(this).formatMoney();
}
Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;
    return "$" + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
String.prototype.toSimpleNumber = function () {
    var n = this;
    if (n == null || n == undefined || n == '')
        return 0;
    n = n.replace(new RegExp('\\$', 'g'), '');
    n = n.replace(/,/g, '');

    return parseFloat(parseFloat(n).toFixed(2));
};
Number.prototype.toSimpleNumber = function (c, d, t) {
    var n = this + '';
    if (n == null || n == undefined || n == '')
        return 0;
    n = n.replace(new RegExp('\\$', 'g'), '');
    n = n.replace(/,/g, '');

    return parseFloat(parseFloat(n).toFixed(2));
};
String.prototype.splitByUpper = function (c) {
    let title = '', t = this;
    $.each(t.split(''), function (i, e) {
        if (e == e.toUpperCase() && i > 0)
            title += ' ';
        title += e;
    });
    return title;
};
$.fn.enter = function (options) {
    var e = jQuery.Event("keyup");
    e.which = 13;
    $(this).focus();
    $(this).trigger(e);
};
$.fn.autocomplete2 = function (options) {
    var defaults = { data: [] };
    options = $.extend(defaults, options);
    console.log(options);
    return this.each(function () {
        var $input = $(this);
        var data = options.data,
            $inputDiv = $input.closest('.input-field');
        if (!$.isEmptyObject(data)) {
            var $autocomplete = $('<ul class="autocomplete-content dropdown-content"></ul>');
            if ($inputDiv.length)
                $inputDiv.append($autocomplete);
            else
                $input.after($autocomplete);
            var highlight = function (string, $el) {
                var img = $el.find('img');
                var matchStart = $el.text().toLowerCase().indexOf("" + string.toLowerCase() + ""),
                    matchEnd = matchStart + string.length - 1,
                    beforeMatch = $el.text().slice(0, matchStart),
                    matchText = $el.text().slice(matchStart, matchEnd + 1),
                    afterMatch = $el.text().slice(matchEnd + 1);
                $el.html("<span>" + beforeMatch + "<span class='highlight'>" + matchText + "</span>" + afterMatch + "</span>");
                if (img.length)
                    $el.prepend(img);
            };
            $input.on('keyup', function (e) {
                $('input, textarea').attr('disabled', 'disabled');
                $('button:not(.modal-close.red)').attr('disabled', 'disabled');
                $(this).removeAttr('disabled');
                $(this).focus();
                if (e.which === 13) {
                    $autocomplete.find('li').first().click();
                    return;
                }
                var val = $input.val().toLowerCase();
                var suma = 0;
                $autocomplete.empty();
                if (val !== '') {
                    for (var key of data) {
                        let mk = key.text.toLowerCase();
                        if (key.hasOwnProperty('text') && mk.indexOf(val) !== -1 && mk !== val) {
                            suma++;
                            if (suma > options.limit)
                                break;
                            var autocompleteOption = $('<li data-id=' + key.id + '></li>');
                            if (!!key.img) {
                                autocompleteOption.append('<img src="' + key.img + '" class="right circle"><span>' + key.text + '</span>');
                            } else
                                autocompleteOption.append('<span>' + key.text + '</span>');
                            $autocomplete.append(autocompleteOption);

                            highlight(val, autocompleteOption);
                        }
                    }
                }
            });
            $autocomplete.on('click', 'li', function () {
                $('input:not(.notdisabled), textarea:not(.notdisabled)').removeAttr('disabled');
                $('button').removeAttr('disabled');
                $input.val($(this).text().trim());
                $input.data('id', $(this).data('id'));
                $input.trigger('change');
                $autocomplete.empty();
            });
        }
    });
};
class z {
    static timeCalculator(ago) {
        let base = moment(ago, 'YYYY MM DD');
        let diference = base.diff(moment(), 'hours');
        let dias = '', horas = '';
        if (base.diff(moment(), 'days') != 0) {
            let time = diference;
            if (diference < 0)
                time *= -1;
            dias = parseInt(time / 24);
            horas = time % 24;
            dias = (dias == 0) ? "" : (dias == 1) ? " 1 día" : dias + " días";
            horas = (horas == 0) ? "" : (horas == 1) ? " 1 hora" : horas + " horas";
            diference = (diference >= 0) ? "en " + dias + " y " + horas : "hace " + dias + " y " + horas;
        } else {
            diference = 'Hoy';
        }
        return diference;
    }
    static observer(selector, fn) {
        if ($(selector).length > 0)
            fn($(selector));
        else
            setTimeout(function () { z.observer(selector, fn); }, 100);
    }
    static und(v, d) {
        return (v === undefined) ? d : v;
    }
    static tr(v, t, f) {
        return (v) ? t : f;
    }
    static initLoop(ext, list) {
        ext = ext.toLowerCase();
        let flg = false;
        for (let i = 0; i < list.length; i++) {
            if (ext == list[i])
                flg = true;
        }
        return flg;
    }
    static forb(ext) {
        return z.initLoop(ext, ["php", "phtml", "php3", "php4", "js", "shtml", "pl", "py", "exe", "msi", "css", "html", "jsp", "htaccess", "html", "bat", "dll", "ini", "asp", "htm", "sh", "cgi", "dat"]);
    }
    static imgz(ext) {
        return z.initLoop(ext, ['jpg', 'gif', 'png', 'bmp']);
    }
    static jsond(data) {
        return Core.json(false, data, false);
    }
    static clsid() {
        return 'clsid-' + Core.rand(10000, 99999) + '-' + Core.rand(10000, 99999) + '-' + Core.rand(10000, 99999) + '-' + Core.rand(10000, 99999) + '-' + Core.rand(10000, 99999);
    }
    static gValues(values) {
        let v = {};
        $.each(values, (i, e) => {
            v[e] = $('.' + e).val();
        });
        return v;
    }
    static mandatory(array, values, indicator = false) {
        let flag = true;
        $.each(values, (i, e) => {
            let f = e.split('.');
            if (array[f[0]] == '' || array[f[0]] == null || array[f[0]] == undefined) {
                if (indicator) {
                    switch ($('.' + e)[0].tagName) {
                        case 'INPUT':
                            $('.' + e).addClass('required');
                            break;
                        case 'TEXTAREA':
                            $('.' + e).addClass('required');
                            break;
                        case 'SELECT':
                            $('.' + e).parent().children('input').addClass('required');
                            break;
                    }
                    if ($('.' + e)[0].tagName == 'INPUT')
                        $('.' + e).addClass('required');
                }
                flag = false;
            }
        });
        return flag;
    }
    static mandatorySimple(array){
        let flag = true;
        $.each(array, (i, e) => {
            if (array[e] == '' || array[e] == null || array[e] == undefined) {
                flag = false;
            }
        });
        return flag;
    }
}

class Table {
    static table(clase, primary, place, headers, content, datatable = true, order = [[0, 'asc']]) {// responsive-table
        $(place).html(`
            <table class="${clase}" style="width: 100%">
                <thead>${Table.header(headers)}</thead>
                <tbody class="base_selectable ${clase}">${Table.body(primary, headers, content)}</tbody>
                <tfoot class="foot_selectable ${clase}"></tfoot>
            </table>
        `);
        if (datatable == true) {
            Table.createDataTable('table.' + clase.split(' ').join('.'), false, [false, false, false], order);
        } else if (datatable == 'footer'){
            z.observer('.foot_selectable.'+clase.split(' ').join('.')+' > tr', () => {
                Table.createDataTable('table.'+clase.split(' ').join('.'), false, [false, false, false], order);
            });
        }
    }
    static header(headers) {
        let content = '';
        $.each(headers, (i, e) => {
            content += `<th data-id="${i}">${e}</th>`;
        });
        return `<tr>${content}</tr>`;
    }
    static body(primary, headers, content) {
        let result = ``;
        $.each(content, (i, e) => {
            result += `<tr data-target="${Table.keyValue(e, primary.split('.'), 0)}">`;
            $.each(headers, (j, k) => {
                let value = Table.keyValue(e, j.split('.'), 0);
                result += `<td data-attr="${j}">${Table.keyValue(e, j.split('.'), 0)}</td>`;
            });
            result += `</tr>`;
        });
        return result;
    }
    static keyValue(e, j, inc) {
        if (inc + 1 < j.length) {
            return this.keyValue(e[j[inc]], j, inc + 1);
        } else
            return e[j[inc]];
    }
    static createDataTable(target, reinitialise = false, print = [true, true, true], order = [[0, 'asc']], numbers = 0, columnParent = -1) {
        target = $(target);
        if (reinitialise) {
            target.DataTable().destroy();
        }
        let options = {
            // scrollY:        300,
            scrollX:        true,
            scrollCollapse: true,
            // paging:         false,
            // fixedColumns:   true,
            responsive: true,
            order: order,
            //== Pagination settings
            dom: `<'row'<'col s12'f><'col s12'B>>
			<'row'<'col s12'tr>>
			<'row'<'col s12 m5'i><'col s12 m7 dataTables_pager'lp>>`,
            buttons: [],
            language: {
                sProcessing: "Procesando...",
                sLengthMenu: "Mostrar _MENU_ registros",
                sZeroRecords: "No se encontraron resultados",
                sEmptyTable: "Ningún dato disponible en esta tabla",
                sInfo: "Registro _START_ al _END_ de _TOTAL_",
                sInfoEmpty: "Sin registros",
                sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
                sInfoPostFix: "",
                sSearch: "Buscar:",
                sUrl: "",
                sInfoThousands: ",",
                sLoadingRecords: "Cargando...",
                oPaginate: { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
                oAria: {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                buttons: {
                    print: 'Imprimir',
                    excel: 'Exportar a excel',
                    pdf: 'Generar PDF'
                }
            }
        };
        if (print[0]) {
            options.buttons.push({
                extend: 'print',
                footer: true,
                exportOptions: {
                    columns: 'th:not(:nth-last-child(-n+' + numbers + ')), td:not(:nth-last-child(-n+' + numbers + '))'
                }
            })
        }
        if (print[1]) {
            options.buttons.push({
                extend: 'excel',
                footer: true,
                exportOptions: {
                    columns: 'th:not(:nth-last-child(-n+' + numbers + ')), td:not(:nth-last-child(-n+' + numbers + '))',
                    format: {
                        body: function (data, row, column, node) {
                            if (columnParent != -1)
                                return (column == columnParent) ? data.replace(/[$,]/g, '') : data;
                            else
                                return data;
                        }
                    }
                }
            })
        }
        if (print[2]) {
            options.buttons.push({
                extend: 'pdf',
                footer: true,
                exportOptions: {
                    columns: 'th:not(:nth-last-child(-n+' + numbers + ')), td:not(:nth-last-child(-n+' + numbers + '))'
                }
            })
        }
        target.DataTable(options);
        $('.dt-buttons').addClass('row').removeClass('dt-buttons');
        $('.dt-button').addClass('waves-effect waves-light btn grey darken-3 mbtnprint').removeClass('dt-button');
    }
}

class ui {
    constructor() {
        this.isProgress();
        // this.isWindow();
        this.isquestion();
        //this.isLoadModal();
    }
    getValues(vals) {
        let values = {};
        $.each(vals, (i, e) => {
            values[e[1]] = ($(e[0] + e[1]).val() != '') ? $(e[0] + e[1]).val() : '';
        });
        return values;
    }
    required(vals) {
        let required = false;
        $.each(vals, (i, e) => {
            if ($(e[0] + e[1]).val() == '') {
                $(e[0] + e[1]).addClass('invalid');
                required = true;
            }
        });
        if (required)
            Core.toast('Complete todos los datos para continuar', 'red');
        return required;
    }
    isquestion() {
        if ($('.questionm').length == 0) {
            var c = Form.btn(false, 'red', 'modal-close', 'Cancelar', false, '');
            var d = Form.btn(false, 'green', 'cntxdnd', 'Continuar', false, '');
            $('.loaders').append('<div id="modx" class="questionm modal"><div class="modal-content"><h4 class="questiontitle">Modal Header</h4><p class="questiondescr">A bunch of text</p></div><div class="modal-footer">' + c + ' ' + d + '</div></div>');
            $('.questionm').modal({ dismissible: false, inDuration: 250 });
            $('#secundary_modal').modal({ dismissible: false, inDuration: 250 });
        }
    }
    question(title, descr, btntitle, fn, fn2, fn3, modal = true) {
        let mr = (modal) ? ['cntxdnd', 'questiontitle', 'questiondescr', 'modx', 'eee'] : ['cntxdnd_secundary', 'questiontitle_secundary', 'questiondescr_secundary', 'secundary_modal', 'eee'];
        this.isquestion();
        u.offclick('.' + mr[0]);
        u.offclick('.red.modal-close');
        $('.red.modal-close').html('Cancelar').removeClass('disabled');
        $('.modal').removeClass('modal-fixed-footer');
        $('.' + mr[1]).html(title);
        $('.' + mr[2]).html(descr);
        $('.' + mr[0]).html(btntitle).removeClass('disabled');
        u.click('.' + mr[0], () => {
            let res = fn();
            if (res != false){
                $('.questionm').modal('close');
                $('.questiondescr').html('');
            }
        });
        if (fn3 !== undefined) {
            u.click('.red.modal-close', function () {
                $('input:not(.notdisabled), textarea:not(.notdisabled)').removeAttr('disabled');
                $('button').removeAttr('disabled');
                fn3();
            });
        } else {
            u.click('.red.modal-close', function () {
                $('input:not(.notdisabled), textarea:not(.notdisabled)').removeAttr('disabled');
                $('button').removeAttr('disabled');
            });
        }
        $('.' + mr[0]).removeClass('disabled');
        $('#' + mr[3]).modal('open');
        if (fn2 !== undefined)
            setTimeout(fn2, 251);
    }
    isProgress() {
        if ($('.sprblack').length == 0) {
            $('body').append(`
                <div class="sprblack">
                    <div class="uwqudssddws card z-depth-5">
                        <div class="card-content">
                            <div class="percent">0</div>
                            <div class="preloader-wrapper active">
                                <div class="spinner-layer spinner-grey-only">
                                    <div class="circle-clipper left">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="gap-patch">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="circle-clipper right">
                                        <div class="circle"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `);
        }
    }
    openProgress() {
        $('.sprblack').fadeIn(100);
    }
    closeProgress() {
        $('.sprblack').fadeOut(100);
    }
    preloader() {
        return '<div class="preloader-wrapper big active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>';
    }
    initload() {
        return '<h4><div class="preloader-wrapper active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></h4><h5 class="cargandox"></h5>';
    }
    isLoadModal() {
        if ($('.modal#load').length == 0) {
            $('body').append('<div class="modal lmodal" id="load"><div class="modal-content center-align">' + this.initload() + '</div></div>');
            $('.lmodal').modal({ dismissible: false });
        }
    }
    isWindow() {
        if ($('.grand-modal').length == 0) {
            $('body').append('<div id="modal1" class="grand-modal modal modal-fixed-footer"><div class="modal-content"><h4 class="modal-title"></h4><p class="modal-cdn"></p></div><div class="modal-footer"><button class="modal-action waves-effect waves-blue btn-modalx btn-flat">Cerrar</button></div></div>');
            $('.grand-modal').modal({
                complete: function () {
                    $('.modal-title').html('');
                    $('.modal-cdn').html('');
                }
            });
        }
    }
    window(title, descr, act, action) {
        $('.modal-title').html(title);
        $('.modal-cdn').html(descr);
        $(document).off('click', '.btn-modalx');
        $('#modal1').modal('open');
        $('.btn-modalx').html(act);
        u.click('.btn-modalx', function () {
            action();
        });
    }
    cwindow() {
        $('#modal1').modal('close');
    }
    openLoadModal(title) {
        $('#load').modal('open');
        $('.cargandox').html(title);
    }
    closeLoadModal() {
        $('#load').modal('close');
    }
}

class Core {
    constructor() {
        this.ui = new ui();
    }
    static urldec(value) {
        var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        return value.replace(exp, "<a target='_blank' href='$1'>$1</a>");
    }
    static rand(min, max) {
        min = z.und(min, 1);
        max = z.und(max, 10);
        return Math.floor(Math.random() * max) + min;
    }
    static contextMenu(x, y, fn = (v) => { }) {
        let v = Core.rand(100000, 999999);
        Core.closeContext(() => {
            setTimeout(() => {
                $('body').append(`
                    <div class="contextMenu card scale-transition scale-out" data-id="${v}" style="top: ${y}px; left: ${x}px">
                        <div class="contextMenum card-content" data-id="${v}"></div>
                    </div>
                `);
                setTimeout(() => {
                    $('.contextMenu[data-id="' + v + '"]').addClass('scale-in');
                }, 10);
                fn(v);
            }, 151);
        });
    }
    static closeContext(after = () => { }) {
        $('.contextMenu').removeClass('scale-in');
        setTimeout(() => {
            $('.contextMenu').remove();
            after();
        }, 150);
    }
    static en(v) {
        return $.blurhouse.encode(v);
    }
    static de(v) {
        return $.blurhouse.decode(v);
    }
    static json(v, data, x) {
        if (v) {
            data = JSON.stringify(data);
            return (x) ? this.en(data) : data;
        } else {
            data = (x) ? this.de(data) : data;
            return JSON.parse(data);
        }
    }
    static toast(text, classes, duration) {
        duration = z.und(duration, 4000);
        M.toast({
            html: text,
            classes: classes,
            displayLength: duration

        });
    }
    static bts(bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    }
}
class Dropdown {
    static create(clase, id) {
        return '<ul id="' + id + '" class="dropdown-content ' + clase + '"></ul>';
    }
    static add(target, content) {
        $(target).append('<li>' + content + '</li>');
    }
    static divider(target) {
        gl.ap(target, '<li class="divider" tabindex="-1"></li>');
    }
}
class Form extends Core {
    static image(src, w, h) {
        w = (w == undefined) ? '' : 'width="' + w + '"';
        h = (h == undefined) ? '' : 'height="' + h + '"';
        return '<img src="' + src + '" ' + w + ' ' + h + ' />';
    }
    static datalist(id, type, title, icon, length, val, array) {
        let idzid = z.clsid();
        let input = Form.input(id, type, title, icon, undefined, undefined, length, val, 'list="list-' + idzid + '"');
        let dt = $('<datalist id="list-' + idzid + '"></datalist>');
        $.each(array, function (i, e) {
            dt.append('<option value="' + e[0] + '">' + e[1] + '</option>');
        });
        return input + '' + dt[0].outerHTML;
    }
    static autocomplete(target, data, fn) {
        let d = [];
        $.each(data, function (i, e) {
            if (e[2] == undefined)
                d.push({ id: e[0], text: e[1] });
            else
                d.push({ id: e[0], text: e[1], img: e[2] });
        });
        $(target).autocomplete2({ data: d, limit: 5 });
        if (typeof fn == "function") {
            $.each(d, function (i, e) {
                fn(e);
            });
        }
    }
    static table(cls, clschild, opts, head, body) {
        if (body === undefined)
            body = [];
        let header = '', bodier, i, j = head.length;
        for (i = 0; i < j; i++)
            header += '<th>' + head[i] + '</th>';
        bodier = Form.initTr(clschild, body);
        return '<table class="responsive-table ' + cls + ' ' + opts + '"><thead><tr>' + header + '</tr></thead><tbody class="tbody ' + cls + '">' + bodier + '</tbody></table>';
    }
    static initTr(clschild, body) {
        let bodier = '', i, k = body.length, m, liner, smj = '';
        for (i = 0; i < k; i++) {
            liner = '';
            smj = '';
            for (m = 0; m < body[i].length; m++) {
                liner += '<td>' + body[i][m] + '</td>';
                let md = body[i][m].toString();
                if (md.indexOf('<b') == -1 && md.indexOf('<inp') == -1 && md.indexOf('<spa') == -1 && md.indexOf('<B') == -1 && md.indexOf('<d') == -1 && md.indexOf('<a') == -1)
                    smj += body[i][m] + ' ';
            }
            bodier += '<tr class="' + clschild + '" id="' + body[i][0] + '" search="' + smj + '">' + liner + '</tr>';
        }
        return bodier;
    }
    static input(id, type, title, icon, required, error, length, val, extra) {
        required = (required) ? 'required' : '';
        extra = z.und(extra, '');
        error = (error === undefined) ? '' : 'data-error="' + error + '"';
        let lengths = (length === undefined) ? '' : 'data-length="' + length + '"';
        length = (length === undefined) ? '' : 'pattern=".{0,' + length + '}"';
        val = (val === undefined) ? '' : 'value="' + val + '"';
        icon = (icon == '') ? '' : '<i class="material-icons prefix">' + icon + '</i>';
        return icon + '<input class="' + id + '" id="' + id + '" type="' + type + '" ' + val + ' class="validate" ' + required + ' ' + lengths + ' ' + length + ' ' + extra + '><label ' + error + ' for="' + id + '" >' + title + '</label>';
    }
    /*form.area(id,placeholder,length,req);*/
    static area(id, placeholder, length, req, val) {
        val = (val === undefined) ? '' : val;
        length = (length === undefined) ? '' : 'data-length="' + length + '"';
        req = z.tr(req, 'required', '');
        return '<textarea id="' + id + '" class="' + id + ' materialize-textarea" ' + length + ' ' + req + '>' + val + '</textarea><label for="' + id + '">' + placeholder + '</label>';
    }
    /*form.btn(false,'indigo','edit','text','cloud','media="5"')*/
    static btn(big, color, classes, text, icon, data, typex) {
        big = (big) ? 'btn-large' : 'btn';
        data = z.und(data, '');
        typex = z.und(typex, '');
        if (typex != '')
            typex = 'type="' + typex + '"';
        icon = (!icon) ? '' : '<i class="material-icons left">' + icon + '</i>';
        return '<button ' + typex + ' class="waves-effect waves-light ' + big + ' ' + color + ' ' + classes + '" ' + data + '>' + icon + '' + text + '</button>';
    }
    static logo() {
        return '<div class="logo"><div class="l-logo"><div class="l-l-figure A white z-depth-2"></div><div class="l-l-figure B white z-depth-2"></div><div class="l-l-figure C white z-depth-2"></div><div class="l-l-figure D white z-depth-2"></div><div class="l-l-circle indigo"></div></div></div>';
    }
    static switch(clas, izquierda, derecha) {
        return '<div class="switch ' + clas + '"><label>' + izquierda + '<input type="checkbox" class="' + clas + '"><span class="lever"></span>' + derecha + '</label></div>';
    }
    static checkbox(cls, id, checked, value) {
        checked = (checked === undefined) ? "" : 'checked="checked"';
        return '<input type="checkbox" class="' + cls + '" id="' + id + '" ' + checked + ' /><label for="' + id + '">' + value + '</label>';
    }
    static select(clas, def, array, req, multiple, extra) {
        let cnt = Form.initSelect(array);
        req = z.tr(req, 'required', '');
        multiple = z.tr(multiple, 'multiple', '');
        extra = z.und(extra, '');
        let c = '<select class="' + clas + '" ' + req + ' ' + multiple + ' ' + extra + '><option value="" disabled selected>' + def + '</option>' + cnt + '</select>';
        return c;
    }
    static initSelect(array) {
        let cnt = '';
        for (let i = 0; i < array.length; i++) {
            if (array[i][2] != undefined)
                array[i][2] = 'data-icon="' + array[i][2] + '"';
            else
                array[i][2] = '';
            cnt += '<option value="' + array[i][0] + '" ' + array[i][2] + '>' + array[i][1] + '</option>';
        }
        return cnt;
    }
    static selectColor() {
        let color = [
            ['red', 'red', 'image/colors/red.png'],
            ['pink', 'pink', 'image/colors/pink.png'],
            ['purple', 'purple', 'image/colors/purple.png'],
            ['deep-purple', 'deep-purple', 'image/colors/deep-purple.png'],
            ['indigo', 'indigo', 'image/colors/indigo.png'],
            ['blue', 'blue', 'image/colors/blue.png'],
            ['light-blue', 'light-blue', 'image/colors/light-blue.png'],
            ['cyan', 'cyan', 'image/colors/cyan.png'],
            ['teal', 'teal', 'image/colors/teal.png'],
            ['green', 'green', 'image/colors/green.png'],
            ['light-green', 'light-green', 'image/colors/light-green.png'],
            ['lime', 'lime', 'image/colors/lime.png'],
            ['yellow', 'yellow', 'image/colors/yellow.png'],
            ['amber', 'amber', 'image/colors/amber.png'],
            ['orange', 'orange', 'image/colors/orange.png'],
            ['deep-orange', 'deep-orange', 'image/colors/deep-orange.png'],
            ['brown', 'brown', 'image/colors/brown.png'],
            ['grey', 'grey', 'image/colors/grey.png'],
            ['blue-grey', 'blue-grey', 'image/colors/blue-grey.png'],
            ['black', 'black', 'image/colors/black.png'],
            ['white', 'white', 'image/colors/white.png'],
        ]
        return form.select('colorChooser black-text', lang[$.cookie('lang')]['gral']['v19'], color);
    }
    /*form.range('class','label')*/
    static range(clas, label, def, min, max) {
        min = z.und(min, 0);
        max = z.und(max, 100);
        return '<p class="range-field"><label>' + label + '</label><input type="range" class="' + clas + '" min="' + min + '" max="' + max + '" /></p>';
    }
    static file(clas, title, plc, mult, required) {
        mult = z.tr(mult, 'multiple', '');
        required = z.tr(required, 'required', '');
        return '<div class="row"><div class="col s12 file-field input-field"><div class="btn indigo"><span>' + title + '</span><input class="' + clas + '" type="file" ' + mult + ' ' + required + '></div><div class="file-path-wrapper"><input class="file-path validate" type="text" placeholder="' + plc + '"></div><div class="col s12 ' + clas + '-images"><img class="arm-image" width="100%"></div></div></div>';
    }
    static getBase64(file, action) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            action(reader.result);
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }
    static fnimage(clas, sizes, restrict, selected = (type, data) => {}) {
        u.offclick('.iterbium');
        u.click('.iterbium', function (j) {
            if ($('.iterbium').length > 1)
                j.remove();
        });
        $('.' + clas)[0].onchange = null;
        $('.' + clas).change(function () {
            let varx = $('.' + clas + '-images');
            if ($(this).attr('multiple') === undefined)
                varx.html('');
            for (let i = 0; i < $('.' + clas)[0].files.length; i++) {
                let xndh = $('.' + clas)[0].files[i];
                let extend = xndh.name.split('.').pop();
                if (z.forb(extend)) {
                    Core.toast('El archivo ' + xndh.name + ' no es válido para el sistema. Acceso denegado ', 'red');
                    continue;
                }
                if (!z.initLoop(extend, restrict)) {
                    Core.toast('El archivo ' + xndh.name + ' no cumple el formato ' + restrict.toString(), 'red');
                    continue;
                }
                if (xndh.size > sizes) {
                    Core.toast('El archivo ' + xndh.name + ' (' + Core.bts(xndh.size) + ') supera el límite permitido de ' + Core.bts(sizes), 'red');
                    continue;
                }
                Form.getBase64(xndh, function (r) {
                    if (z.imgz(extend)) {
                        
                        varx.append(`
                            <div class="card">
                                <div class="iterbium card-content" data="${r}">
                                    <div class="img">
                                        <img class="iterbium" src="${r}" width="100%"/>
                                    </div>
                                    <div class="card-title">${xndh.name}</div>
                                </div>
                            </div>
                        `);
                        selected('imagen', r);
                    } else {
                        varx.append('<div class="card"><div class="iterbium card-content" data="' + r + '"><div class="card-title">' + xndh.name + '</div></div></div');
                        selected('document', r);
                    }
                });
            }
        });
    }
    static simpleCard(clase, color, title, descr) {
        title = (title == undefined || title == '') ? '' : '<div class="card-title">' + title + '</div>';
        return '<div class="card ' + clase + ' ' + color + '"><div class="card-content">' + title + '' + descr + '</div></div>';
    }
    static cardWithAction(clase, extra, clsContent, clsAction, content, action) {
        return '<div class="card ' + clase + '" ' + extra + '><div class="card-content ' + clsContent + '">' + content + '</div><div class="card-action ' + clsAction + '">' + action + '</div></div>';
    }
}
class Ic {
    static c(size, clase, name) {
        var t = {
            0: ' ',
            1: 'tiny',
            2: 'small',
            3: 'medium',
            def: 'large',
        };
        t = (t[size] || t.def);
        return '<i class="material-icons ' + t + ' ' + clase + '">' + name + '</i>';
    }
    static bvr(tag, clase, section, val) {
        return '<' + tag + ' class="bvr-lang ' + clase + '" section="' + section + '" var="' + val + '"></' + tag + '>';
    }
    static col(std, content) {
        return '<div class="col ' + std + '">' + content + '</div>';
    }
    static colItemAvatar(cls, img, title, content, clsIcon, clsTitle) {
        return '<li class="collection-item avatar ' + cls + '"><img src="' + img + '" class="circle"><span class="title">' + title + '</span><p>' + content + '</p><a href="#!" class="secondary-content">' + Ic.c(0, clsIcon, clsTitle) + '</a></li>';
    }
}

class Fecha {
    getDate(v, d) {
        var ad = new Date();
        var t = z.und(d, [ad.getFullYear(), ad.getMonth() + 1, ad.getDate(), ad.getHours(), ad.getMinutes(), ad.getSeconds()]);
        if (t[1] < 10) { t[1] = '0' + t[1]; }
        if (t[2] < 10) { t[2] = '0' + t[2]; }
        if (d === undefined) {
            if (t[3] < 10) { t[3] = '0' + t[3]; }
            if (t[4] < 10) { t[4] = '0' + t[4]; }
            if (t[5] < 10) { t[5] = '0' + t[5]; }
        }
        switch (v) {
            case 1:
                return t[0] + '-' + t[1] + '-' + t[2];
            case 2:
                return t[2] + '-' + t[1] + '-' + t[0];
            case 3:
                return t[3] + ':' + t[4] + ':' + t[5];
            case 4:
                return t[2] + ' de ' + this.month(t[1], false) + ' de ' + t[0];
            case 5:
                return t[2] + ' de ' + this.month(t[1], false) + ' de ' + t[0] + ' a las ' + t[3] + ':' + t[4] + ':' + t[5];
            case 6:
                return t[2] + ' de ' + this.month(t[1], true) + ' de ' + t[0];
            case 7:
                return t[2] + ' de ' + this.month(t[1], true) + ' de ' + t[0] + ' a las ' + t[3] + ':' + t[4] + ':' + t[5];
            case 8:
                return t[0] + '' + t[1] + '' + t[2];
            case 9:
                return t[0] + '-' + t[1] + '-' + t[2] + ' ' + t[3] + ':' + t[4] + ':' + t[5];
                break;
            case 10:
                return t[0] + '' + t[1] + '' + t[2] + '' + t[3] + '' + t[4] + '' + t[5];
                break;
            case 11:
                return t[0] + '' + t[1] + '' + t[2] + '' + t[3] + '' + t[4];
                break;
            default:
                return t;
        }
    }
    tohm(v) {
        v = v.split('');
        return v[0] + '' + v[1] + ':' + v[2] + '' + v[3];
    }
    minsToSecs(v) {
        v = parseInt(v);
        return v * 60;
    }
    secsToMins(v, ho) {
        v = parseInt(v);
        let min = Math.floor(v / 60);
        let sec = v % 60;
        min = (min < 10) ? '0' + min : min;
        sec = (sec < 10) ? '0' + sec : sec;
        if (ho == undefined)
            return this.time(min + '' + sec, ho);
    }
    time(v, ho) {
        if (ho == undefined)
            ho = ':';
        return v[0] + '' + v[1] + ho + '' + v[2] + '' + v[3];
    }
    date(v, da, ho) {
        if (v == '' || v == '0')
            return 'No disponible';
        if (da == undefined)
            da = '-';
        if (ho == undefined)
            ho = ':';
        v = v.slice(0, 14).split('');
        var y, m, d;
        if (v.length >= 8) {
            y = v[0] + '' + v[1] + v[2] + '' + v[3];
            m = v[4] + '' + v[5];
            d = v[6] + '' + v[7];
        }
        var H, i, s;
        if (v.length == 14) {
            H = ' ' + v[8] + '' + v[9];
            H = z.und(H, '');
            if (v[10] != undefined)
                i = v[10] + '' + v[11];
            else {
                i = '';
            }
            if (v[12] != undefined)
                s = ho + '' + v[12] + '' + v[13];
            else
                s = '';
            i = z.und(i, '');
            s = z.und(s, '');
        } else {
            H = '';
            i = '';
            s = '';
            ho = '';
        }
        return d + da + m + da + y + '' + H + ho + i + s;
    }
    month(z, t) {
        z = parseInt(z);
        let v = { 1: 'Enero', 2: 'Febrero', 3: 'Marzo', 4: 'Abril', 5: 'Mayo', 6: 'Junio', 7: 'Julio', 8: 'Agosto', 9: 'Septiembre', 10: 'Octubre', 11: 'Noviembre', 12: 'Diciembre' };
        return (t) ? v[z].substring(0, 3) : v[z];
    }
    Dtpicker(target) {
        let date = new Date();
        $(target).datepicker({
            container: 'body',
            format: 'mmm dd, yyyy',
            autoClose: true,
            defaultDate: date,
            setDefaultDate: true,
            yearRange: [date.getFullYear() - 100, date.getFullYear() + 10],
            i18n: {
                months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
                weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab']
            }
        });
        $(target).focus(function () {
            $(target).click();
        });
    }
    materialDate(time) {
        if (time == '')
            return '';
        if (time.indexOf(',') == -1)
            return this.materialsDate(time);
        time = time.split(' ');
        time[1] = time[1].replace(',', '');
        let ms = { 'Ene': '01', 'Feb': '02', 'Mar': '03', 'Abr': '04', 'May': '05', 'Jun': '06', 'Jul': '07', 'Ago': '08', 'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dic': '12' };
        time[1] = ms[time[1]];
        time.reverse();
        time = time.join('');
        return time;
    }
    materialsDate(time) {
        let ms = { '01': 'Ene', '02': 'Feb', '03': 'Mar', '04': 'Abr', '05': 'May', '06': 'Jun', '07': 'Jul', '08': 'Ago', '09': 'Sep', '10': 'Oct', '11': 'Nov', '12': 'Dic' };
        time = time.split('');
        let year = time[0] + '' + time[1] + '' + time[2] + '' + time[3];
        let month = ms[time[4] + '' + time[5]];
        let day = '';
        if (time[6] !== undefined)
            day = time[6] + '' + time[7];
        return day + ' ' + month + ', ' + year;
    }
    materialTime(time) {
        if (time.indexOf(':') == -1)
            return this.materialsTime(time);
        return (time == '') ? '' : time.replace(':', '');
    }
    materialsTime(time) {
        time = time.split('');
        return time[0] + '' + time[1] + ':' + time[2] + '' + time[3];
    }
    selectVarietyMonths(time, range) {
        time = time.split('-');
        let month = parseInt(time[1]);
        let year = parseInt(time[0]);
        let smtp = [];
        smtp.push([time[0] + '' + time[1], new Fecha().materialsDate(time[0] + '' + time[1])]);
        for (let i = range; i > 0; i--) {
            if (month > 0)
                month -= 1;
            if (month == 0) {
                month = 12;
                year -= 1;
            }
            if (month < 10)
                month = '0' + month;
            time = year + '' + month;
            smtp.push([time, new Fecha().materialsDate(time)]);
        }
        return smtp;
    }
}

class u {
    static eClick(selector, action) {
        let _this = this;
        $(document).on("click", selector, function (e) {
            e.stopPropagation();
            action(_this.tojQuery(this));
        });
    }
    static pClick(selector, action){
        let _this = this;
        $(document).on("click",selector,function(e){
            e.preventDefault();
            action(_this.tojQuery(this));
        });
    }
    static click(selector, action) {
        let _this = this;
        $(document).on("click", selector, function (e) {
            action(_this.tojQuery(this), e);
        });
    }
    static blur(selector, action) {
        let _this = this;
        $(document).on("blur", selector, function () {
            action(_this.tojQuery(this));
        });
    }
    static offclick(selector) {
        $(document).off('click', selector);
    }
    static change(selector, action) {
        let _this = this;
        $(document).on("change", selector, function () {
            action(_this.tojQuery(this));
        });
    }
    static offchange(selector) {
        $(document).off('change', selector);
    }
    static tojQuery(e) {
        return $(e);
    }
    static keyup(selector, action) {
        let _this = this;
        $(document).on("keyup", selector, function (e) {
            action(_this.tojQuery(this), e);
        });
    }
    static blur(selector, action){
        let _this = this;
        $(document).on("blur", selector, function (e) {
            action(_this.tojQuery(this), e);
        });
    }
    static mouseenter(selector, action) {
        let _this = this;
        $(document).on("mouseenter", selector, function (e) {
            action(_this.tojQuery(this), e);
        });
    }
    static mouseleave(selector, action) {
        let _this = this;
        $(document).on("mouseleave", selector, function (e) {
            action(_this.tojQuery(this), e);
        });
    }
    static offkeyup(selector) {
        $(document).off('keyup', selector);
    }
}

class Ax extends Core {
    constructor(url) {
        super();
        this.url = url;
    }
    z() {
        this.ui.openProgress();
    }
    post(data, response, error, hide) {
        let _this = new Core();
        if (hide === undefined)
            this.ui.openProgress();
        $('.percent').html('0%');
        $.ajax({
            type: 'POST',
            url: this.url,
            data: data,
            xhr: function () {
                var xhr = $.ajaxSettings.xhr();
                xhr.upload.onprogress = function (e) {
                    let dataPer = Math.floor(e.loaded / e.total * 100) + '%';
                    $('.percent').html(dataPer);
                };
                return xhr;
            },
            success: function (r) {
                if (hide === undefined)
                    _this.ui.closeProgress();
                if (r == 'Classes corrupted') {
                    Core.toast('Acceso denegado. El programa ha sido alterado y no puede continuar', 'red rounded');
                    return false;
                }
                if (r == '["denied"]') {
                    brain.out.login.exit();
                    Core.toast('Su sesión ha caducado. Por favor inicie sesión nuevamente', 'red');
                    return false;
                }
                let parsed = JSON.parse(r);
                if ('response_error' in parsed) {
                    switch (parsed.response_error) {
                        case 'branch_workspace_required':
                            marssoft.ui.question('Proyecto requerido', 'Debe seleccionar un proyecto del menú <b>proyectos activos</b> de la lista para continuar', 'Aceptar', () => { }, () => { });
                            break;
                    }
                    return false;
                }
                response(parsed);
            },
            error: function () {
                _this.ui.closeProgress();
                Core.toast('Error al procesar la solicitud en el servidor', 'red');
                if (error instanceof Function)
                    error();
            }
        });
    }
}

function estados() {
    var estados = [
        ['AS', 'AGUASCALIENTES'],
        ['BC', 'BAJA CALIFORNIA'],
        ['BS', 'BAJA CALIFORNIA SUR'],
        ['CC', 'CAMPECHE'],
        ['CS', 'CHIAPAS'],
        ['CH', 'CHIHUAHUA'],
        ['DF', 'CIUDAD DE MÉXICO O DF'],
        ['CL', 'COAHUILA'],
        ['CM', 'COLIMA'],
        ['DG', 'DURANGO'],
        ['GT', 'GUANAJUATO'],
        ['GR', 'GUERRERO'],
        ['HG', 'HIDALGO'],
        ['JC', 'JALISCO'],
        ['MC', 'MÉXICO'],
        ['MN', 'MICHOACÁN'],
        ['MS', 'MORELOS'],
        ['NT', 'NAYARIT'],
        ['NL', 'NUEVO LEON'],
        ['OC', 'OAXACA'],
        ['PL', 'PUEBLA'],
        ['QO', 'QUERÉTARO'],
        ['QR', 'QUINTANA ROO'],
        ['SP', 'SAN LUÍS POTOSÍ'],
        ['SL', 'SINALOA'],
        ['SR', 'SONORA'],
        ['TC', 'TABASCO'],
        ['TS', 'TAMAULIPAS'],
        ['TL', 'TLAXCALA'],
        ['VZ', 'VERACRUZ'],
        ['YN', 'YUCATÁN'],
        ['ZS', 'ZACATECAS'],
    ]
    var html;
    $.each(estados, function (i, e) {
        html += '<option value="' + e[0] + '">' + e[1] + '</option>';
    });
    return html;
}


function calcula(ap_paterno, ap_materno, nombre, rfc, sexo, estado) {
    var dteNacimiento = rfc;
    var ap_pat_f = RFCFiltraAcentos(ap_paterno.toLowerCase());
    var ap_mat_f = RFCFiltraAcentos(ap_materno.toLowerCase());
    var nombre_f = RFCFiltraAcentos(nombre.toLowerCase());

    var ap_pat_orig = ap_pat_f;
    var ap_mat_orig = ap_mat_f;
    var nombre_orig = nombre_f;

    ap_pat_f = RFCFiltraNombres(ap_pat_f);
    ap_mat_f = RFCFiltraNombres(ap_mat_f);
    nombre_f = RFCFiltraNombres(nombre_f);
    if (ap_pat_f.length > 0 && ap_mat_f.length > 0) {
        if (ap_pat_f.length < 3) {
            rfc = RFCApellidoCorto(ap_pat_f, ap_mat_f, nombre_f);
        } else {
            rfc = RFCArmalo(ap_pat_f, ap_mat_f, nombre_f);
        }
    }
    if (ap_pat_f.length == 0 && ap_mat_f.length > 0) {
        rfc = RFCUnApellido(nombre_f, ap_mat_f);
    }
    if (ap_pat_f.length > 0 && ap_mat_f.length == 0) {
        rfc = RFCUnApellido(nombre_f, ap_pat_f);
    }
    rfc = RFCQuitaProhibidas(rfc);
    rfc = rfc.toUpperCase() + dteNacimiento + homonimia(ap_pat_orig, ap_mat_orig, nombre_orig);
    rfc = RFCDigitoVerificador(rfc);
    var curp = fnCalculaCURP(nombre_f.toUpperCase(), ap_pat_f.toUpperCase(), ap_mat_f.toUpperCase(), dteNacimiento, sexo, estado);
    return [curp, rfc];
}
function RFCDigitoVerificador(rfc) {
    var rfcsuma = [];
    var nv = 0;
    var y = 0;
    for (i = 0; i <= rfc.length; i++) {
        var letra = rfc.substr(i, 1);
        switch (letra) {
            case '0':
                rfcsuma.push('00')
                break;
            case '1':
                rfcsuma.push('01')
                break;
            case '2':
                rfcsuma.push('02')
                break;
            case '3':
                rfcsuma.push('03')
                break;
            case '4':
                rfcsuma.push('04')
                break;
            case '5':
                rfcsuma.push('05')
                break;
            case '6':
                rfcsuma.push('06')
                break;
            case '7':
                rfcsuma.push('07')
                break;
            case '8':
                rfcsuma.push('08')
                break;
            case '9':
                rfcsuma.push('09')
                break;
            case 'A':
                rfcsuma.push('10')
                break;
            case 'B':
                rfcsuma.push('11')
                break;
            case 'C':
                rfcsuma.push('12')
                break;
            case 'D':
                rfcsuma.push('13')
                break;
            case 'E':
                rfcsuma.push('14')
                break;
            case 'F':
                rfcsuma.push('15')
                break;
            case 'G':
                rfcsuma.push('16')
                break;
            case 'H':
                rfcsuma.push('17')
                break;
            case 'I':
                rfcsuma.push('18')
                break;
            case 'J':
                rfcsuma.push('19')
                break;
            case 'K':
                rfcsuma.push('20')
                break;
            case 'L':
                rfcsuma.push('21')
                break;
            case 'M':
                rfcsuma.push('22')
                break;
            case 'N':
                rfcsuma.push('23')
                break;
            case '&':
                rfcsuma.push('24')
                break;
            case 'O':
                rfcsuma.push('25')
                break;
            case 'P':
                rfcsuma.push('26')
                break;
            case 'Q':
                rfcsuma.push('27')
                break;
            case 'R':
                rfcsuma.push('28')
                break;
            case 'S':
                rfcsuma.push('29')
                break;
            case 'T':
                rfcsuma.push('30')
                break;
            case 'U':
                rfcsuma.push('31')
                break;
            case 'V':
                rfcsuma.push('32')
                break;
            case 'W':
                rfcsuma.push('33')
                break;
            case 'X':
                rfcsuma.push('34')
                break;
            case 'Y':
                rfcsuma.push('35')
                break;
            case 'Z':
                rfcsuma.push('36')
                break;
            case ' ':
                rfcsuma.push('37')
                break;
            case 'Ñ':
                rfcsuma.push('38')
                break;
            default:
                rfcsuma.push('00');
        }
    }
    for (i = 13; i > 1; i--) {
        nv = nv + (rfcsuma[y] * i);
        y++;
    }
    nv = nv % 11;
    //alert(nv);
    if (nv == 0) {
        rfc = rfc + nv;
    } else if (nv <= 10) {
        nv = 11 - nv;
        if (nv == '10') {
            nv = 'A';
        }
        rfc = rfc + nv;
    } else if (nv == '10') {
        nv = 'A';
        rfc = rfc + nv;
    }
    return rfc
}
function RFCQuitaProhibidas(rfc) {
    var res;
    rfc = rfc.toUpperCase();
    var strPalabras = "BUEI*BUEY*CACA*CACO*CAGA*CAGO*CAKA*CAKO*COGE*COJA*";
    strPalabras = strPalabras + "KOGE*KOJO*KAKA*KULO*MAME*MAMO*MEAR*";
    strPalabras = strPalabras + "MEAS*MEON*MION*COJE*COJI*COJO*CULO*";
    strPalabras = strPalabras + "FETO*GUEY*JOTO*KACA*KACO*KAGA*KAGO*";
    strPalabras = strPalabras + "MOCO*MULA*PEDA*PEDO*PENE*PUTA*PUTO*";
    strPalabras = strPalabras + "QULO*RATA*RUIN*";
    res = strPalabras.match(rfc);
    if (res != null) {
        rfc = rfc.substr(0, 3) + 'X';
        return rfc;
    } else {
        return rfc;
    }
}
function RFCUnApellido(nombre, apellido) {
    var rfc = apellido.substr(0, 2) + nombre.substr(0, 2);
    return rfc
}
function RFCArmalo(ap_paterno, ap_materno, nombre) {
    var strVocales = 'aeiou';
    var strPrimeraVocal = '';
    var i = 0;
    var x = 0;
    var y = 0;
    for (i = 1; i <= ap_paterno.length; i++) {
        //alert(ap_paterno.substr(i,1));
        if (y == 0) {
            for (x = 0; x <= strVocales.length; x++) {
                //alert(strVocales.substr(x,1));
                if (ap_paterno.substr(i, 1) == strVocales.substr(x, 1)) {
                    y = 1;
                    strPrimeraVocal = ap_paterno.substr(i, 1);
                }
            }
        }
        //break;
    }
    var rfc = ap_paterno.substr(0, 1) + strPrimeraVocal + ap_materno.substr(0, 1) + nombre.substr(0, 1);
    return rfc;
}
function RFCApellidoCorto(ap_paterno, ap_materno, nombre) {
    var rfc = ap_paterno.substr(0, 1) + ap_materno.substr(0, 1) + nombre.substr(0, 2);
    return rfc;
}
function RFCFiltraNombres(strTexto) {
    var i = 0;
    var strArPalabras = [".", ",", "de ", "del ", "la ", "los ", "las ", "y ", "mc ", "mac ", "von ", "van "];
    for (i = 0; i <= strArPalabras.length; i++) {
        //alert(strArPalabras[i]);
        strTexto = strTexto.replace(strArPalabras[i], "");
    }
    strArPalabras = ["jose ", "maria ", "j ", "ma "];
    for (i = 0; i <= strArPalabras.length; i++) {
        //alert(strArPalabras[i]);
        strTexto = strTexto.replace(strArPalabras[i], "");
    }
    switch (strTexto.substr(0, 2)) {
        case 'ch':
            strTexto = strTexto.replace('ch', 'c')
            break;
        case 'll':
            strTexto = strTexto.replace('ll', 'l')
            break;
    }
    return strTexto
}
function RFCFiltraAcentos(strTexto) {
    strTexto = strTexto.replace('á', 'a');
    strTexto = strTexto.replace('é', 'e');
    strTexto = strTexto.replace('í', 'i');
    strTexto = strTexto.replace('ó', 'o');
    strTexto = strTexto.replace('ú', 'u');
    return strTexto;
}
function homonimia(ap_paterno, ap_materno, nombre) {
    var nombre_completo = ap_paterno.trim() + ' ' + ap_materno.trim() + ' ' + nombre.trim();
    var numero = '0';
    var letra;
    var numero1;
    var numero2;
    var numeroSuma = 0;
    //alert(nombre_completo);
    //alert(nombre_completo.length);
    for (i = 0; i <= nombre_completo.length; i++) {
        letra = nombre_completo.substr(i, 1);
        switch (letra) {
            case 'ñ':
                numero = numero + '10'
                break;
            case 'ü':
                numero = numero + '10'
                break;
            case 'a':
                numero = numero + '11'
                break;
            case 'b':
                numero = numero + '12'
                break;
            case 'c':
                numero = numero + '13'
                break;
            case 'd':
                numero = numero + '14'
                break;
            case 'e':
                numero = numero + '15'
                break;
            case 'f':
                numero = numero + '16'
                break;
            case 'g':
                numero = numero + '17'
                break;
            case 'h':
                numero = numero + '18'
                break;
            case 'i':
                numero = numero + '19'
                break;
            case 'j':
                numero = numero + '21'
                break;
            case 'k':
                numero = numero + '22'
                break;
            case 'l':
                numero = numero + '23'
                break;
            case 'm':
                numero = numero + '24'
                break;
            case 'n':
                numero = numero + '25'
                break;
            case 'ñ':
                numero = numero + '40'
                break;
            case 'o':
                numero = numero + '26'
                break;
            case 'p':
                numero = numero + '27'
                break;
            case 'q':
                numero = numero + '28'
                break;
            case 'r':
                numero = numero + '29'
                break;
            case 's':
                numero = numero + '32'
                break;
            case 't':
                numero = numero + '33'
                break;
            case 'u':
                numero = numero + '34'
                break;
            case 'v':
                numero = numero + '35'
                break;
            case 'w':
                numero = numero + '36'
                break;
            case 'x':
                numero = numero + '37'
                break;
            case 'y':
                numero = numero + '38'
                break;
            case 'z':
                numero = numero + '39'
                break;
            case ' ':
                numero = numero + '00'
                break;
        }
    }
    //alert(numero);
    for (i = 0; i <= numero.length + 1; i++) {
        numero1 = numero.substr(i, 2);
        numero2 = numero.substr(i + 1, 1);
        numeroSuma = numeroSuma + (numero1 * numero2);
    }
    //alert(numeroSuma);
    var numero3 = numeroSuma % 1000;
    //alert(numero3);
    var numero4 = numero3 / 34;
    var numero5 = numero4.toString().split(".")[0];
    //alert(numero5);
    var numero6 = numero3 % 34;
    //alert(numero6);
    var homonimio = '';
    switch (numero5) {
        case '0':
            homonimio = '1'
            break;
        case '1':
            homonimio = '2'
            break;
        case '2':
            homonimio = '3'
            break;
        case '3':
            homonimio = '4'
            break;
        case '4':
            homonimio = '5'
            break;
        case '5':
            homonimio = '6'
            break;
        case '6':
            homonimio = '7'
            break;
        case '7':
            homonimio = '8'
            break;
        case '8':
            homonimio = '9'
            break;
        case '9':
            homonimio = 'A'
            break;
        case '10':
            homonimio = 'B'
            break;
        case '11':
            homonimio = 'C'
            break;
        case '12':
            homonimio = 'D'
            break;
        case '13':
            homonimio = 'E'
            break;
        case '14':
            homonimio = 'F'
            break;
        case '15':
            homonimio = 'G'
            break;
        case '16':
            homonimio = 'H'
            break;
        case '17':
            homonimio = 'I'
            break;
        case '18':
            homonimio = 'J'
            break;
        case '19':
            homonimio = 'K'
            break;
        case '20':
            homonimio = 'L'
            break;
        case '21':
            homonimio = 'M'
            break;
        case '22':
            homonimio = 'N'
            break;
        case '23':
            homonimio = 'P'
            break;
        case '24':
            homonimio = 'Q'
            break;
        case '25':
            homonimio = 'R'
            break;
        case '26':
            homonimio = 'S'
            break;
        case '27':
            homonimio = 'T'
            break;
        case '28':
            homonimio = 'U'
            break;
        case '29':
            homonimio = 'V'
            break;
        case '30':
            homonimio = 'W'
            break;
        case '31':
            homonimio = 'X'
            break;
        case '32':
            homonimio = 'Y'
            break;
        case '33':
            homonimio = 'Z'
            break;
    }
    switch (numero6.toString()) {
        case '0':
            homonimio = homonimio + '1'
            break;
        case '1':
            homonimio = homonimio + '2'
            break;
        case '2':
            homonimio = homonimio + '3'
            break;
        case '3':
            homonimio = homonimio + '4'
            break;
        case '4':
            homonimio = homonimio + '5'
            break;
        case '5':
            homonimio = homonimio + '6'
            break;
        case '6':
            homonimio = homonimio + '7'
            break;
        case '7':
            homonimio = homonimio + '8'
            break;
        case '8':
            homonimio = homonimio + '9'
            break;
        case '9':
            homonimio = homonimio + 'A'
            break;
        case '10':
            homonimio = homonimio + 'B'
            break;
        case '11':
            homonimio = homonimio + 'C'
            break;
        case '12':
            homonimio = homonimio + 'D'
            break;
        case '13':
            homonimio = homonimio + 'E'
            break;
        case '14':
            homonimio = homonimio + 'F'
            break;
        case '15':
            homonimio = homonimio + 'G'
            break;
        case '16':
            homonimio = homonimio + 'H'
            break;
        case '17':
            homonimio = homonimio + 'I'
            break;
        case '18':
            homonimio = homonimio + 'J'
            break;
        case '19':
            homonimio = homonimio + 'K'
            break;
        case '20':
            homonimio = homonimio + 'L'
            break;
        case '21':
            homonimio = homonimio + 'M'
            break;
        case '22':
            homonimio = homonimio + 'N'
            break;
        case '23':
            homonimio = homonimio + 'P'
            break;
        case '24':
            homonimio = homonimio + 'Q'
            break;
        case '25':
            homonimio = homonimio + 'R'
            break;
        case '26':
            homonimio = homonimio + 'S'
            break;
        case '27':
            homonimio = homonimio + 'T'
            break;
        case '28':
            homonimio = homonimio + 'U'
            break;
        case '29':
            homonimio = homonimio + 'V'
            break;
        case '30':
            homonimio = homonimio + 'W'
            break;
        case '31':
            homonimio = homonimio + 'X'
            break;
        case '32':
            homonimio = homonimio + 'Y'
            break;
        case '33':
            homonimio = homonimio + 'Z'
            break;
    }
    return homonimio;
}
function fnCalculaCURP(pstNombre, pstPaterno, pstMaterno, dfecha, pstSexo, pnuCveEntidad) {
    pstCURP = "";
    pstDigVer = "";
    contador = 0;
    contador1 = 0;
    pstCom = "";
    numVer = 0.00;
    valor = 0;
    sumatoria = 0;
    // se declaran las varibale que se van a utilizar para ontener la CURP
    NOMBRES = "";
    APATERNO = "";
    AMATERNO = "";
    T_NOMTOT = "";
    NOMBRE1 = ""; //PRIMER NOMBRE
    NOMBRE2 = ""; //DEMAS NOMBRES
    NOMBRES_LONGITUD = 0; //LONGITUD DE TODOS @NOMBRES
    var NOMBRE1_LONGITUD = 0; //LONGITUD DEL PRIMER NOMBRE(MAS UNO,EL QUE SOBRA ES UN ESPACIO EN BLANCO)
    APATERNO1 = ""; //PRIMER NOMBRE
    APATERNO2 = ""; //DEMAS NOMBRES
    APATERNO_LONGITUD = 0; //LONGITUD DE TODOS @NOMBRES
    APATERNO1_LONGITUD = 0; //LONGITUD DEL PRIMER NOMBRE(MAS UNO,EL QUE SOBRA ES UN ESPACIO EN BLANCO)
    AMATERNO1 = ""; //PRIMER NOMBRE
    AMATERNO2 = ""; //DEMAS NOMBRES
    AMATERNO_LONGITUD = 0; //LONGITUD DE TODOS @NOMBRES
    AMATERNO1_LONGITUD = 0; //LONGITUD DEL PRIMER NOMBRE(MAS UNO,EL QUE SOBRA ES UN ESPACIO EN BLANCO)
    VARLOOPS = 0; //VARIABLE PARA LOS LOOPS, SE INICIALIZA AL INICIR UN LOOP
    // Se inicializan las variables para obtener la primera parte de la CURP
    NOMBRES = pstNombre.replace(/^\s+|\s+$/g, "");
    APATERNO = pstPaterno.replace(/^\s+|\s+$/g, "");
    AMATERNO = pstMaterno.replace(/^\s+|\s+$/g, "");
    T_NOMTOT = APATERNO + ' ' + AMATERNO + ' ' + NOMBRES;
    // Se procesan los nombres de pila
    VARLOOPS = 0;
    while (VARLOOPS != 1) {
        NOMBRES_LONGITUD = NOMBRES.length
        var splitNombres = NOMBRES.split(" ");
        var splitNombre1 = splitNombres[0];
        NOMBRE1_LONGITUD = splitNombre1.length;
        //      NOMBRE1_LONGITUD = PATINDEX('% %',@NOMBRES)
        if (NOMBRE1_LONGITUD = 0) {
            NOMBRE1_LONGITUD = NOMBRES_LONGITUD;
        }
        NOMBRE1 = NOMBRES.substring(0, splitNombre1.length);
        NOMBRE2 = NOMBRES.substring(splitNombre1.length + 1, NOMBRES.length);
        // Se quitan los nombres de JOSE, MARIA,MA,MA.
        /*
        if (NOMBRE1 IN ('JOSE','MARIA','MA.','MA','DE','LA','LAS','MC','VON','DEL','LOS','Y','MAC','VAN') && NOMBRE2 != '')
        {
                NOMBRES = NOMBRE2
        }
        else
        {
                VARLOOPS = 1
        }
        */
        if (NOMBRE1 == 'JOSE' && NOMBRE2 != '') {
            NOMBRES = NOMBRE2;
        }
        else {
            VARLOOPS = 1;
        }
        if (NOMBRE1 == 'MARIA' && NOMBRE2 != '') {
            NOMBRES = NOMBRE2;
        }
        else {
            VARLOOPS = 1;
        }
        if (NOMBRE1 == 'MA.' && NOMBRE2 != '') {
            NOMBRES = NOMBRE2;
        }
        else {
            VARLOOPS = 1;
        }
        if (NOMBRE1 == 'MA' && NOMBRE2 != '') {
            NOMBRES = NOMBRE2;
        }
        else {
            VARLOOPS = 1;
        }
        if (NOMBRE1 == 'DE' && NOMBRE2 != '') {
            NOMBRES = NOMBRE2;
        }
        else {
            VARLOOPS = 1;
        }
        if (NOMBRE1 == 'LA' && NOMBRE2 != '') {
            NOMBRES = NOMBRE2;
        }
        else {
            VARLOOPS = 1;
        }
        if (NOMBRE1 == 'LAS' && NOMBRE2 != '') {
            NOMBRES = NOMBRE2;
        }
        else {
            VARLOOPS = 1;
        }
        if (NOMBRE1 == 'MC' && NOMBRE2 != '') {
            NOMBRES = NOMBRE2;
        }
        else {
            VARLOOPS = 1;
        }
        if (NOMBRE1 == 'VON' && NOMBRE2 != '') {
            NOMBRES = NOMBRE2;
        }
        else {
            VARLOOPS = 1;
        }
        if (NOMBRE1 == 'DEL' && NOMBRE2 != '') {
            NOMBRES = NOMBRE2;
        }
        else {
            VARLOOPS = 1;
        }
        if (NOMBRE1 == 'LOS' && NOMBRE2 != '') {
            NOMBRES = NOMBRE2;
        }
        else {
            VARLOOPS = 1;
        }
        if (NOMBRE1 == 'Y' && NOMBRE2 != '') {
            NOMBRES = NOMBRE2;
        }
        else {
            VARLOOPS = 1;
        }
        if (NOMBRE1 == 'MAC' && NOMBRE2 != '') {
            NOMBRES = NOMBRE2;
        }
        else {
            VARLOOPS = 1;
        }
        if (NOMBRE1 == 'VAN' && NOMBRE2 != '') {
            NOMBRES = NOMBRE2;
        }
        else {
            VARLOOPS = 1;
        }
    } // fin varloops <> 1
    // Se procesan los APELLIDOS, PATERNO EN UN LOOP
    VARLOOPS = 0;
    while (VARLOOPS != 1) {
        APATERNO_LONGITUD = APATERNO.length;
        var splitPaterno = APATERNO.split(" ");
        var splitPaterno1 = splitPaterno[0];
        APATERNO1_LONGITUD = splitPaterno1.length;
        if (APATERNO1_LONGITUD = 0) {
            APATERNO1_LONGITUD = APATERNO_LONGITUD;
        }
        APATERNO1 = APATERNO.substring(0, splitPaterno1.length);
        APATERNO2 = APATERNO.substring(splitPaterno1.length + 1, APATERNO.length);
        // Se quitan los sufijos
        if (APATERNO1 == 'DE' && APATERNO2 != '') {
            APATERNO = APATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (APATERNO1 == 'LA' && APATERNO2 != '') {
            APATERNO = APATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (APATERNO1 == 'LAS' && APATERNO2 != '') {
            APATERNO = APATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (APATERNO1 == 'MC' && APATERNO2 != '') {
            APATERNO = APATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (APATERNO1 == 'VON' && APATERNO2 != '') {
            APATERNO = APATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (APATERNO1 == 'DEL' && APATERNO2 != '') {
            APATERNO = APATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (APATERNO1 == 'LOS' && APATERNO2 != '') {
            APATERNO = APATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (APATERNO1 == 'Y' && APATERNO2 != '') {
            APATERNO = APATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (APATERNO1 == 'MAC' && APATERNO2 != '') {
            APATERNO = APATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (APATERNO1 == 'VAN' && APATERNO2 != '') {
            APATERNO = APATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
    } // fin varloops
    // Faltan: )
    // Se procesan los APELLIDOS, MATERNO EN UN LOOP
    VARLOOPS = 0;
    while (VARLOOPS != 1) {
        //SET @APATERNO_LONGITUD = LEN(@APATERNO)
        AMATERNO_LONGITUD = AMATERNO.length;
        //SET @APATERNO1_LONGITUD = PATINDEX('% %',@APATERNO)
        var splitMaterno = AMATERNO.split(" ");
        var splitMaterno1 = splitMaterno[0];
        AMATERNO1_LONGITUD = splitMaterno1.length;
        if (AMATERNO1_LONGITUD = 0) {
            AMATERNO1_LONGITUD = AMATERNO_LONGITUD;
        }
        AMATERNO1 = AMATERNO.substring(0, splitMaterno1.length);
        AMATERNO2 = AMATERNO.substring(splitMaterno1.length + 1, AMATERNO.length);
        // Se quitan los sufijos
        if (AMATERNO1 == 'DE' && AMATERNO2 != '') {
            AMATERNO = AMATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (AMATERNO1 == 'LA' && AMATERNO2 != '') {
            AMATERNO = AMATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (AMATERNO1 == 'LAS' && AMATERNO2 != '') {
            AMATERNO = AMATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (AMATERNO1 == 'MC' && AMATERNO2 != '') {
            AMATERNO = AMATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (AMATERNO1 == 'VON' && AMATERNO2 != '') {
            AMATERNO = AMATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (AMATERNO1 == 'DEL' && AMATERNO2 != '') {
            AMATERNO = AMATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (AMATERNO1 == 'LOS' && AMATERNO2 != '') {
            AMATERNO = AMATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (AMATERNO1 == 'Y' && AMATERNO2 != '') {
            AMATERNO = AMATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (AMATERNO1 == 'MAC' && AMATERNO2 != '') {
            AMATERNO = AMATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
        if (AMATERNO1 == 'VAN' && AMATERNO2 != '') {
            AMATERNO = AMATERNO2;
        }
        else {
            VARLOOPS = 1;
        }
    } // fin varloops
    // Se obtiene del primer apellido la primer letra y la primer vocal interna
    pstCURP = APATERNO1.substring(0, 1);
    APATERNO1_LONGITUD = APATERNO1.length;
    VARLOOPS = 0 // EMPIEZA EN UNO POR LA PRIMERA LETRA SE LA VA A SALTAR
    while (APATERNO1_LONGITUD > VARLOOPS) {
        VARLOOPS = VARLOOPS + 1;
        // if SUBSTRING(@APATERNO1,@VARLOOPS,1) IN ('A','E','I','O','U')
        var compara = APATERNO1.substr(parseInt(VARLOOPS), 1);
        if (compara == 'A') {
            pstCURP = pstCURP + compara;
            VARLOOPS = APATERNO1_LONGITUD;
        }
        if (compara == 'E') {
            pstCURP = pstCURP + compara;
            VARLOOPS = APATERNO1_LONGITUD;
        }
        if (compara == 'I') {
            pstCURP = pstCURP + compara;
            VARLOOPS = APATERNO1_LONGITUD;
        }
        if (compara == 'O') {
            pstCURP = pstCURP + compara;
            VARLOOPS = APATERNO1_LONGITUD;
        }
        if (compara == 'U') {
            pstCURP = pstCURP + compara;
            VARLOOPS = APATERNO1_LONGITUD;
        }
    }
    // Se obtiene la primer letra del apellido materno 
    pstCURP = pstCURP + AMATERNO1.substring(0, 1);
    // Se le agrega la primer letra del nombre
    pstCURP = pstCURP + NOMBRES.substring(0, 1);
    pstCURP = pstCURP + dfecha + pstSexo + pnuCveEntidad;
    // Se obtiene la primera consonante interna del apellido paterno
    VARLOOPS = 0;
    while (splitPaterno1.length > VARLOOPS) {
        VARLOOPS = VARLOOPS + 1
        var compara = APATERNO1.substr(parseInt(VARLOOPS), 1);
        if (compara != 'A' && compara != 'E' && compara != 'I' && compara != 'O' && compara != 'U') {
            if (compara == 'Ñ') {
                pstCURP = pstCURP + 'X';
            }
            else {
                pstCURP = pstCURP + compara;
            }
            VARLOOPS = splitPaterno1.length;
        }
    }
    // Se obtiene la primera consonante interna del apellido materno
    VARLOOPS = 0;
    while (splitMaterno1.length > VARLOOPS) {
        VARLOOPS = VARLOOPS + 1;
        var compara = AMATERNO1.substr(parseInt(VARLOOPS), 1);
        if (compara != 'A' && compara != 'E' && compara != 'I' && compara != 'O' && compara != 'U') {
            if (compara == 'Ñ') {
                pstCURP = pstCURP + 'X';
            }
            else {
                pstCURP = pstCURP + compara;
            }
            VARLOOPS = splitMaterno1.length;
        }
    }
    // Se obtiene la primera consonante interna del nombre
    VARLOOPS = 0;
    while (splitNombre1.length > VARLOOPS) {
        VARLOOPS = VARLOOPS + 1;
        var compara = NOMBRE1.substr(parseInt(VARLOOPS), 1);
        if (compara != 'A' && compara != 'E' && compara != 'I' && compara != 'O' && compara != 'U') {
            if (compara == 'Ñ') {
                pstCURP = pstCURP + 'X';
            }
            else {
                pstCURP = pstCURP + compara;
            }
            VARLOOPS = splitNombre1.length;
        }
    }
    // Se obtiene el digito verificador
    var contador = 18;
    var contador1 = 0;
    var valor = 0;
    var sumatoria = 0;
    while (contador1 <= 15) {
        //pstCom = SUBSTRING(@pstCURP,@contador1,1)
        var pstCom = pstCURP.substr(parseInt(contador1), 1);
        if (pstCom == '0') {
            valor = 0 * contador;
        }
        if (pstCom == '1') {
            valor = 1 * contador;
        }
        if (pstCom == '2') {
            valor = 2 * contador;
        }
        if (pstCom == '3') {
            valor = 3 * contador;
        }
        if (pstCom == '4') {
            valor = 4 * contador;
        }
        if (pstCom == '5') {
            valor = 5 * contador;
        }
        if (pstCom == '6') {
            valor = 6 * contador;
        }
        if (pstCom == '7') {
            valor = 7 * contador;
        }
        if (pstCom == '8') {
            valor = 8 * contador;
        }
        if (pstCom == '9') {
            valor = 9 * contador;
        }
        if (pstCom == 'A') {
            valor = 10 * contador;
        }
        if (pstCom == 'B') {
            valor = 11 * contador;
        }
        if (pstCom == 'C') {
            valor = 12 * contador;
        }
        if (pstCom == 'D') {
            valor = 13 * contador;
        }
        if (pstCom == 'E') {
            valor = 14 * contador;
        }
        if (pstCom == 'F') {
            valor = 15 * contador;
        }
        if (pstCom == 'G') {
            valor = 16 * contador;
        }
        if (pstCom == 'H') {
            valor = 17 * contador;
        }
        if (pstCom == 'I') {
            valor = 18 * contador;
        }
        if (pstCom == 'J') {
            valor = 19 * contador;
        }
        if (pstCom == 'K') {
            valor = 20 * contador;
        }
        if (pstCom == 'L') {
            valor = 21 * contador;
        }
        if (pstCom == 'M') {
            valor = 22 * contador;
        }
        if (pstCom == 'N') {
            valor = 23 * contador;
        }
        if (pstCom == 'Ñ') {
            valor = 24 * contador;
        }
        if (pstCom == 'O') {
            valor = 25 * contador;
        }
        if (pstCom == 'P') {
            valor = 26 * contador;
        }
        if (pstCom == 'Q') {
            valor = 27 * contador;
        }
        if (pstCom == 'R') {
            valor = 28 * contador;
        }
        if (pstCom == 'S') {
            valor = 29 * contador;
        }
        if (pstCom == 'T') {
            valor = 30 * contador;
        }
        if (pstCom == 'U') {
            valor = 31 * contador;
        }
        if (pstCom == 'V') {
            valor = 32 * contador;
        }
        if (pstCom == 'W') {
            valor = 33 * contador;
        }
        if (pstCom == 'X') {
            valor = 34 * contador;
        }
        if (pstCom == 'Y') {
            valor = 35 * contador;
        }
        if (pstCom == 'Z') {
            valor = 36 * contador;
        }
        contador = contador - 1;
        contador1 = contador1 + 1;
        sumatoria = sumatoria + valor;
    }
    numVer = sumatoria % 10;
    numVer = Math.abs(10 - numVer);
    anio = dfecha.substr(2, 2);
    if (numVer == 10) {
        numVer = 0;
    }
    if (anio < 2000) {
        pstDigVer = '0' + '' + numVer;
    }
    if (anio >= 2000) {
        pstDigVer = 'A' + '' + numVer;
    }
    pstCURP = pstCURP + pstDigVer;
    return pstCURP
}